﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	// got the majority of this from Brackey's
	// https://github.com/Brackeys/2D-Character-Controller/blob/master/CharacterController2D.cs
	
	
	[SerializeField] private float m_JumpForce = 400f;
	[SerializeField] private float m_HighJumpForce = 450f;// Amount of force added when the player jumps.
	[SerializeField] private float _moveSpeed = 10f;
	[Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;	// How much to smooth out the movement
	[SerializeField] private bool m_AirControl = false;							// Whether or not a player can steer while jumping;
	[SerializeField] private LayerMask m_WhatIsGround;							// A mask determining what is ground to the character
	[SerializeField] private Transform m_GroundCheck;							// A position marking where to check if the player is grounded.
	[SerializeField] private float _burnDistance = 5f;
	
	const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
	private bool m_Grounded;            // Whether or not the player is grounded.
	private Rigidbody2D m_Rigidbody2D;
	private bool m_FacingRight = true;  // For determining which way the player is currently facing.
	private Vector3 m_Velocity = Vector3.zero;
	private bool _rightMovementHindered = false;
	private bool _leftMovementhindered = false;
	private bool _invisible = false;
	private bool _highJump = false;
	
	// set by RFID
	private bool _canInvisible = false;
	private bool _canHighJump = false;
	private bool _canBurn = false;
	

	private void Awake()
	{
		m_Rigidbody2D = GetComponent<Rigidbody2D>();
	}

	private void Update()
	{
		Movement();

		ToggleInvisibility();

		ToggleHighJump();

		Burn();

	}

	private void FixedUpdate()
	{
		bool wasGrounded = m_Grounded;
		m_Grounded = false;

		// The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
		// This can be done using layers instead but Sample Assets will not overwrite your project settings.
		Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
		for (int i = 0; i < colliders.Length; i++)
		{
			if (colliders[i].gameObject != gameObject)
			{
				m_Grounded = true;
			}
		}
	}


	public void Move(float move, bool jump)
	{
		Debug.Log("I tried to move");
		//only control the player if grounded or airControl is turned on
		if (m_Grounded || m_AirControl)
		{
			// Move the character by finding the target velocity
			Vector3 targetVelocity = new Vector2(move * 10f, m_Rigidbody2D.velocity.y);
			// And then smoothing it out and applying it to the character
			m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

			// If the input is moving the player right and the player is facing left...
			if (move > 0 && !m_FacingRight)
			{
				// ... flip the player.
				Flip();
			}
			// Otherwise if the input is moving the player left and the player is facing right...
			else if (move < 0 && m_FacingRight)
			{
				// ... flip the player.
				Flip();
			}
		}
		// If the player should jump...
		if (m_Grounded && jump)
		{
			// Add a vertical force to the player.
			m_Grounded = false;
			if (!_highJump)
				m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
			else
				m_Rigidbody2D.AddForce(new Vector2(0f, m_HighJumpForce));
		}
	}

	private void Movement()
	{
		m_Rigidbody2D.drag = 0;
		bool inputApplied = false;
		
		if (Input.GetKeyDown(KeyCode.Space))
		{
			inputApplied = true;
			if (Input.GetKey(KeyCode.A) && !_leftMovementhindered)
			{
				Move(-_moveSpeed, true);
			}
			else if (Input.GetKey(KeyCode.D) && !_rightMovementHindered)
			{
				Move(_moveSpeed,  true);
			}
			else
			{
				Move(0, true);
			}
		}
		else
		{
			if (Input.GetKey(KeyCode.A) && !_leftMovementhindered)
			{
				inputApplied = true;
				Move(-_moveSpeed, false);
			}
			else if (Input.GetKey(KeyCode.D) && !_rightMovementHindered)
			{
				inputApplied = true;
				Move(_moveSpeed,  false);
			}
		}
		
		if ((inputApplied || Math.Abs(m_Rigidbody2D.velocity.x) > .1f) && m_Grounded)
			m_Rigidbody2D.drag = 5;
	}

	private void ToggleInvisibility()
	{
		if (Input.GetKeyDown(KeyCode.S) && _canInvisible)
		{
			_invisible = !_invisible;
		}
	}

	private void ToggleHighJump()
	{
		if (Input.GetKeyDown(KeyCode.E) && _canHighJump)
		{
			_highJump = !_highJump;
		}
	}
	private void Burn()
	{
		if (Input.GetKeyDown(KeyCode.R) && _canBurn)
		{
			List<GameObject> burnableObjects = GetComponent<BurnableObjects>().GetBurnableObjects();
			for (int i = burnableObjects.Count - 1; i >= 0; i--)
			{
				if (Vector3.Distance(transform.position, burnableObjects[i].transform.position) < _burnDistance)
				{
					burnableObjects[i].GetComponent<Burn>().BurnObj();
					burnableObjects.Remove(burnableObjects[i]);
				}
			}
		}
	}
	private void Flip()
	{
		// Switch the way the player is labelled as facing.
		m_FacingRight = !m_FacingRight;

		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	public void SetRightMovementHindered(bool hindered)
	{
		_rightMovementHindered = hindered;
	}

	public void SetLeftMovementHindered(bool hindered)
	{
		_leftMovementhindered = hindered;
	}

	public bool GetInvisible()
	{
		return _invisible;
	}

	public void SetAbilityOne()
	{
		_canInvisible = true;
	}
	
	public void SetAbilityTwo()
	{
		_canHighJump = true;
	}
	
	public void SetAbilityThree()
	{
		_canBurn = true;
	}
}
