﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurnableObjects : MonoBehaviour
{
    [SerializeField] private List<GameObject> _burnableObjects = new List<GameObject>();

    public List<GameObject> GetBurnableObjects()
    {
        return _burnableObjects;
    }
}
