﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private GameObject _player;

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = new Vector3(_player.transform.position.x, transform.position.y, -10);
    }
}
