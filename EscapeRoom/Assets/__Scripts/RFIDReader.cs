﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Phidget22;
using Phidget22.Events;

public class RFIDReader : MonoBehaviour
{
    [SerializeField] private Player _playerScript;
    private RFID _chip = new RFID();

    private String _rfid1 = "45001391e4";
    private String _rfid2 = "450013af1c";
    private String _rfid3 = "4500139d87";

    // Update is called once per frame
    void Start()
    {
        _chip.Tag += onTag;

        try
        {
            Debug.Log("I opened");
            _chip.Open();
        }
        catch (PhidgetException ex)
        {
            Debug.LogException(ex);
        }
    }

    private void Update()
    {
        try
        {
            Debug.Log(_chip.Attached);
        }
        catch (PhidgetException e)
        {
            Debug.Log(e.Message);
        }
    }

    private void onTag(object sender, RFIDTagEventArgs e)
    {
        if (e.Tag.Equals(_rfid1))
            _playerScript.SetAbilityOne();
        else if (e.Tag.Equals(_rfid2))
            _playerScript.SetAbilityTwo();
        else if (e.Tag.Equals(_rfid3))
            _playerScript.SetAbilityThree();
    }
}
