﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardInteraction : MonoBehaviour
{
    [SerializeField] private bool _blockRight = true;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            bool invisible = other.gameObject.GetComponent<Player>().GetInvisible();
            if (invisible)
                return;
            
            if (_blockRight)
                other.gameObject.GetComponent<Player>().SetRightMovementHindered(true);
            else
                other.gameObject.GetComponent<Player>().SetLeftMovementHindered(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (_blockRight)
                other.gameObject.GetComponent<Player>().SetRightMovementHindered(false);
            else
                other.gameObject.GetComponent<Player>().SetLeftMovementHindered(false);
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            bool invisible = other.gameObject.GetComponent<Player>().GetInvisible();
            if (invisible)
            {
                if (_blockRight)
                    other.gameObject.GetComponent<Player>().SetRightMovementHindered(false);
                else
                    other.gameObject.GetComponent<Player>().SetLeftMovementHindered(false);
            }
            else
            {
                if (_blockRight)
                    other.gameObject.GetComponent<Player>().SetRightMovementHindered(true);
                else
                    other.gameObject.GetComponent<Player>().SetLeftMovementHindered(true);
            }
        }
    }
}
